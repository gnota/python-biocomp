n = input('Dame un número entero entre 1 y 10: ')
n = int(n)

file_name = 'tabla-' + str(n) + '.txt'

f = open(file_name, 'w')

for i in range(1, 11):
    f.write(str(n) + ' x ' + str(i) + '=' + str(n * i) + '\n')

f.close()
